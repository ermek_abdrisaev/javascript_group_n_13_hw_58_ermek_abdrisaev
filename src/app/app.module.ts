import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { UserService } from './shared/user.service';
import { UserComponent } from './user/user.component';
import { AddUserComponent } from './user/add-user/add-user.component';
import { GroupComponent } from './group/group.component';
import { CreateGroupComponent } from './group/create-group/create-group.component';
import { UsersComponent } from './user/users/users.component';
import { GroupItemComponent } from './group/group-item/group-item.component';
import { GroupService } from './shared/group.service';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    AddUserComponent,
    GroupComponent,
    CreateGroupComponent,
    UsersComponent,
    GroupItemComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
  ],
  providers: [UserService, GroupService],
  bootstrap: [AppComponent]
})
export class AppModule { }
