import { Component, Input } from '@angular/core';
import { UserGroup } from '../../shared/group.item.model';

@Component({
  selector: 'app-group-item',
  templateUrl: './group-item.component.html',
  styleUrls: ['./group-item.component.css']
})
export class GroupItemComponent {

 @Input() userGroup!: UserGroup;


}
