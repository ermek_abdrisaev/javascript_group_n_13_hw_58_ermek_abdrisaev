import { Component, EventEmitter, Input, Output } from '@angular/core';
import { UserGroup } from '../shared/group.item.model';
import { GroupService } from '../shared/group.service';
import { User } from '../shared/user.model';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.css']
})
export class GroupComponent {
  @Input() user!: User;
  @Output() userAddToGroup = new EventEmitter<User>();

  @Input() userGroups!: UserGroup[];
  @Output() groupAdd = new EventEmitter<UserGroup>();

  constructor(public groupService: GroupService){}

  onGroupClick(user: User){
    this.userAddToGroup.emit(user);
    console.log('click on group');
  }
}
