import { Component, ElementRef, EventEmitter, Output, ViewChild } from '@angular/core';
import { UserGroup } from '../../shared/group.item.model';
import { UserService } from '../../shared/user.service';
import { GroupService } from '../../shared/group.service';

@Component({
  selector: 'app-create-group',
  templateUrl: './create-group.component.html',
  styleUrls: ['./create-group.component.css']
})
export class CreateGroupComponent {


  @ViewChild('groupInput') groupInput!: ElementRef;

  constructor(
    public userService: UserService,
    public groupService: GroupService,
  ){}

  onSubmit(){
    const groupName: string = this.groupInput.nativeElement.value;

    const userGroup = new UserGroup(groupName);
    this.groupService.userGroups.push(userGroup);
    console.log(this.groupService.userGroups)
  }



}
