import { Component, EventEmitter, Output } from '@angular/core';
import { User } from '../../shared/user.model';
import { ROLE } from '../../shared/roles.model';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent {
  @Output() newUser = new EventEmitter<User>();

  userName = '';
  email = '';
  status = false;
  roles = ROLE;
  role = '';

  onSubmit(event: Event){
    event.preventDefault();
    const user = new User(this.userName, this.email, this.status, this.role);
    this.newUser.emit(user);
  }

  formIsEmpty(){
    return this.userName === '' || this.email === '';
  }

}
