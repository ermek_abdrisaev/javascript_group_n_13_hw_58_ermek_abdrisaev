import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { User } from '../../shared/user.model';
import { UserService } from '../../shared/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users!: User[];
  
  @Output() userAddedToGroup = new EventEmitter<User>();
  constructor(private userService: UserService) { }

  ngOnInit(){
    this.users = this.userService.getUsers();
  }

  onUserClick(user: User) {
    this.userAddedToGroup.emit(user);
  }
}
