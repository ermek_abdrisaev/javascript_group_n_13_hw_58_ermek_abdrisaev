import { Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from '../shared/user.model';
import { UserService } from '../shared/user.service';
import { GroupService } from '../shared/group.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent{

  @Input() user!: User;
  @Output() userClick = new EventEmitter<User>();

  constructor(public userService: UserService, groupService: GroupService){}

  onClick(){
    this.userClick.emit(this.user);
  }
}
