import { Component } from '@angular/core';
import { User } from './shared/user.model';
import { UserService } from './shared/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  users: User[] = [];

  constructor(public userService: UserService){};

  onNewUser(user: User){
    this.userService.users.push(user);
  }

  // onUserAddedToGroup(user: User) {
  //   const userGroup = new UserGroup(user);
  //   this.userGroups.push(userGroup);
  // }



}
