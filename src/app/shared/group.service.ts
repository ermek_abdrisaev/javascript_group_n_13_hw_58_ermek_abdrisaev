import { UserGroup } from './group.item.model';

export class GroupService {
  public userGroups: UserGroup[] = [
    new UserGroup('Baseball'),
    new UserGroup('Soccer'),
    new UserGroup('Poker'),
    new UserGroup('Beer'),
  ];
}
