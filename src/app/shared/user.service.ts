import { User } from './user.model';

export class UserService {

  users: User[] = [
    new User('Bob Dillan', 'b.dillan@inbox.ru', true, 'admin'),
    new User('Bob Marley', 'b.marley@rastabox.kz', false, 'user'),
    new User('Scooby Doo', 'doggyDoo@pochta.uz', true, 'redactor')
  ];

  getUsers(){
    return this.users.slice();
  }

  // addUser(user: User){
  //   this.users.push(user);
  // }

}
